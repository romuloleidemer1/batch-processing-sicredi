package br.com.leidemer.batch.configuration;

import java.io.IOException;
import java.io.Writer;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.integration.async.AsyncItemProcessor;
import org.springframework.batch.integration.async.AsyncItemWriter;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.FlatFileHeaderCallback;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import br.com.leidemer.batch.dao.entity.Conta;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

	@Autowired
	public JobBuilderFactory jobBuilderFactory;

	@Autowired
	public StepBuilderFactory stepBuilderFactory;
	
	//Endereco do local do arquivo de origem através do Argumento "--accountFilePath="
	@Value("${accountFilePath}")
	private String accountFilePath;

	private Resource outputResource = new FileSystemResource("/home/rleidemer/data/sicredi/outputData.csv");
	
	@Bean
	public FlatFileItemReader<Conta> reader() {
		//Metodo utilizado para realizar a leitura de arquivos flat. Neste caso CSV.
		//Configuro que o arquivo será limitado e Nomeio os fields.
		//Os fields serao utilizados prlo ContaFieldSetMapper
		//Pulamos a primeira linha "Header"
		//Indico qual será a classe base para o fieldSetMapper. "Conta.class"
		return new FlatFileItemReaderBuilder<Conta>().name("contaItemReader")
				.resource(new FileSystemResource(accountFilePath))
				.delimited()
				.names(new String[] { "agencia", "conta", "saldo", "status" })
				.lineMapper(lineMapper())
				.linesToSkip(1)
				.fieldSetMapper(new BeanWrapperFieldSetMapper<Conta>() {
					{
						setTargetType(Conta.class);
					}
				}).build();
	}

	@Bean
	public LineMapper<Conta> lineMapper() {
		//Configuro como sera realizado o parse de cada linha
		//Defino que sera utilizado o ";" 
		//O numero do resultado do Parse não devera ser igual ao numero de colunas setStrict(false)
		//Atraves do ContaFieldSetMapper Mapeio a leitura do CSV para objeto
		final DefaultLineMapper<Conta> defaultLineMapper = new DefaultLineMapper<>();
		final DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
		lineTokenizer.setDelimiter(";");
		lineTokenizer.setStrict(false);
		lineTokenizer.setNames(new String[] { "agencia", "conta", "saldo", "status" });

		final ContaFieldSetMapper fieldSetMapper = new ContaFieldSetMapper();
		defaultLineMapper.setLineTokenizer(lineTokenizer);
		defaultLineMapper.setFieldSetMapper(fieldSetMapper);

		return defaultLineMapper;
	}

	@Bean
	public ContaProcessor processor() {
		//Responsavel por processo os dados das Contas
		return new ContaProcessor();
	}

	@Bean
	public FlatFileItemWriter<Conta> writerFlat() {
		//Metodo para criar o arquivo de saida
		FlatFileItemWriter<Conta> writer = new FlatFileItemWriter<>();
		writer.setResource(outputResource);

		//Todos os jobs poderao escrever no mesmo arquivo.
		writer.setAppendAllowed(true);

		//Nomeio os campos que serao escritos no arquivo e seu delimitador
		writer.setLineAggregator(new DelimitedLineAggregator<Conta>() {
			{
				setDelimiter(";");
				setFieldExtractor(new BeanWrapperFieldExtractor<Conta>() {
					{
						setNames(new String[] { "agencia", "conta", "saldo", "status", "resultado" });
					}
				});
			}
		});

		//Metodo para criar o header de um arquivo
		writer.setHeaderCallback(new FlatFileHeaderCallback() {
			public void writeHeader(Writer writer) throws IOException {
				writer.write("agencia;conta;saldo;status;resultado");
			}
		});

		return writer;
	}
	
	@Bean
	public TaskExecutor taskExecutor() {
		//Utilizo ThreadPoolTaskExecutor para configurar os workers
		//Possibilita varias threads processarem os registros do CSV
	    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
	    executor.setCorePoolSize(100);
	    executor.setMaxPoolSize(100);
	    executor.setQueueCapacity(100);
	    executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
	    executor.setThreadNamePrefix("MultiThreaded-");
	    return executor;
	}
	
	@Bean
    public ItemProcessor<Conta, Future<Conta>> asyncItemProcessor(){
		//Defino que o Item Processor sera Asyncrono. Possibilitando trabalhar de forma independente
		//Utilizo o Future para receber o resultado de um processamento Asyncrono
        AsyncItemProcessor<Conta, Conta> asyncItemProcessor = new AsyncItemProcessor<>();
        asyncItemProcessor.setDelegate(processor());
        asyncItemProcessor.setTaskExecutor(taskExecutor());
        return asyncItemProcessor;
    }
	
	@Bean
    public ItemWriter<Future<Conta>> asyncItemWriter(){
		//Defino que o Item Writer sera Asyncrono. Possibilitando trabalhar de forma independente
		//Utilizo o Future para receber o resultado de um processamento Asyncrono
        AsyncItemWriter<Conta> asyncItemWriter = new AsyncItemWriter<>();
        asyncItemWriter.setDelegate(writerFlat());
        return asyncItemWriter;
    }

	@Bean
	public Job importContaJob(NotificationListener listener, Step processar) {
		return jobBuilderFactory
				.get("importContaJob")
				.incrementer(new RunIdIncrementer())
				.listener(listener)
				.flow(processar)
				.end()
				.build();
	}

	@Bean
	public Step processar(FlatFileItemWriter<Conta> writer) {
		//Determino a quantidade de registros processado por thread
		//Determino os demais metodos de processamento
		return stepBuilderFactory
				.get("processar")
				.<Conta, Future<Conta>>chunk(800)
				.reader(reader())
				.processor(asyncItemProcessor())
				.writer(asyncItemWriter())
				.taskExecutor(taskExecutor())
				.build();
	}
}
