package br.com.leidemer.batch.configuration;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.stereotype.Component;

import br.com.leidemer.batch.dao.entity.Conta;

@Component
public class ContaFieldSetMapper implements FieldSetMapper<Conta> {

    @Override
    public Conta mapFieldSet(FieldSet fieldSet) {
        final Conta conta = new Conta();

        conta.setAgencia(fieldSet.readString("agencia"));
        conta.setConta(fieldSet.readString("conta"));
        conta.setSaldo(fieldSet.readString("saldo"));
        conta.setStatus(fieldSet.readString("status"));
        return conta;

    }
}
