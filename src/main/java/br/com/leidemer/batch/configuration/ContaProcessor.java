package br.com.leidemer.batch.configuration;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import org.springframework.batch.item.ItemProcessor;

import br.com.leidemer.batch.dao.entity.Conta;
import br.com.leidemer.batch.dao.service.ReceitaService;

public class ContaProcessor implements ItemProcessor<Conta, Conta>{
	
	private static String SUCESSO = "SUCESSO";
	private static String ERRO_PROCESSO = "ERRO NO PROCESSO DA RECEITA";
	private static String ERRO_SALDO = "ERRO NA FORMATACAO DO SALDO";
	private static String ERRO_INESPERADO = "ERRO NA FORMATACAO DO SALDO";

    @Override
    public Conta process(final Conta conta) {
    	//Processo o registro e retorno o resultado para escrita
        final String agencia = conta.getAgencia();
        final String contaField = conta.getConta().replace("-", "");
        final String saldo = conta.getSaldo();
        final String status = conta.getStatus();

        final Conta processedConta = new Conta();
        processedConta.setAgencia(agencia);
        processedConta.setConta(contaField);
        processedConta.setSaldo(saldo);
        processedConta.setStatus(status);
        processedConta.setResultado(SUCESSO);
        
        Double parsedSaldo = null;
        
        NumberFormat nf = NumberFormat.getInstance(new Locale("pt", "BR"));
        try {
        	parsedSaldo = nf.parse(saldo).doubleValue();
		} catch (ParseException e) {
			processedConta.setResultado(ERRO_SALDO);
			e.printStackTrace();
		}
        
        if (!processedConta.getResultado().equals(ERRO_SALDO)) {
	        ReceitaService receitaService = new ReceitaService();
	        try {
	        	if (!receitaService.atualizarConta(agencia, contaField, parsedSaldo, status)) {
	        		processedConta.setResultado("ERRO");
	        	}
			} catch (RuntimeException e) {
				processedConta.setResultado("ERRO INESPERADO");
				e.printStackTrace();
			} catch (InterruptedException e) {
				processedConta.setResultado("ERRO INESPERADO");
				e.printStackTrace();
			}
        }
        
        return processedConta;
    }
}
